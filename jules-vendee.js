const name = "jules-vendee"
const promo = "B2A"

const q1 = `
Select * 
From Track
Where Milliseconds < (Select Milliseconds From Track Where TrackId = 3457)`

const q2 = `
Select * 
From Track
Where MediaTypeId = (Select MediaTypeId From Track Where Name = 'Rehab')`

const q3 = `
Select p.playlistId, p.name, COUNT(t.trackId) as 'Nb tracks', 
SUM(t.Milliseconds) as 'total time', AVG(t.milliseconds) as 'avg time'
from Playlist p 
join PlaylistTrack pt on p.PlaylistId = pt.PlaylistId
join Track t on pt.trackid = t.TrackId
Group by p.PlaylistId , p.Name`

const q4 = `
select pp.PlaylistId, pp.Name, sum(tt.Milliseconds) as time_playlist
from Playlist pp
join PlaylistTrack ptpt on pp.PlaylistId = ptpt.PlaylistId
join Track tt on ptpt.TrackId = tt.TrackId
where (select sum(tt2.Milliseconds) as time_playlist
		from Playlist pp2
		join PlaylistTrack ptpt2 on pp2.PlaylistId = ptpt2.PlaylistId
		join Track tt2 on ptpt2.TrackId = tt2.TrackId
		where pp.PlaylistId = pp2.playlistid) >= (Select AVG((d.total_time_playlist) / (nb.nb_track_playlist)) as AVG_time_playlist
									from Playlist p1
									join (Select p2.PlaylistId, sum(t1.milliseconds) as total_time_playlist
										from Playlist p2
										join PlaylistTrack pt1 on p2.PlaylistId = pt1.PlaylistId
										join Track t1 on pt1.trackid = t1.TrackId
										Group by p2.PlaylistId) d on p1.PlaylistId = d.playlistid
									join (select pt2.playlistid, count(t2.trackid) as nb_track_playlist
										from track t2 join PlaylistTrack pt2 on pt2.TrackId = t2.TrackId
										group by pt2.playlistid) nb on p1.PlaylistId = nb.playlistid)
group by pp.PlaylistId, pp.Name`

const q5 = ``

const q6 = `
Select Distinct c.customerid, c.FirstName, c.LastName
from invoice i
join Customer c on c.CustomerId = i.CustomerId
where i.Total > (Select max(Total)
					from Invoice
          where BillingCountry like 'France')`
          
const q7 = ``
const q8 = `
Select t.TrackId, t.Name, t.Composer, t.Milliseconds, t.Bytes, t.UnitPrice, m.MediaTypeId, m.Name as Name_media,
(select AVG(UnitPrice) from Track) as prix_moyen_global, AVG(UnitPrice) as prix_moyen_media
from track t
join mediatype m on m.MediaTypeId = t.MediaTypeId
where unitprice > (select AVG(UnitPrice) from Track)
Group by  t.TrackId, t.Name, t.Composer, t.Milliseconds, t.Bytes, t.UnitPrice, m.MediaTypeId, m.Name`
const q9 = ``
const q10 = `
Select p.playlistid, p.name, COUNT(ar.ArtistId) Nb_Artist,
COUNT(t.TrackId) / COUNT(ar.artistId) 'Nb_Musique/Artiste',
AVG(i.unitprice) 'prix_moyen', MAX(ar.Name),  COUNT(ar.ArtistId) Max_Artist
from playlist p
join PlaylistTrack pt on pt.PlaylistId = p.PlaylistId
join Track t on t.TrackId = pt.TrackId
join InvoiceLine i on t.TrackId = i.TrackId
join Album a on a.AlbumId = t.AlbumId
join Artist ar on a.ArtistId = ar.ArtistId
group by p.PlaylistId, p.Name`

const q11 = `
select c.Country, COUNT(c.country)+ COUNT(i.billingcountry)+COUNT(e.country) 'Nb ref pays'
from Employee e
join Customer c on c.SupportRepId = e.EmployeeId 
join Invoice i on i.CustomerId = c.CustomerId
group by c.Country`

const q12 = `
select c.Country, COUNT(c.country)+ COUNT(i.billingcountry)+COUNT(e.country) 'Total',
COUNT(e.country) Employee, COUNT(c.country) Customer, COUNT(i.billingcountry) Invoice
from Employee e
join Customer c on c.SupportRepId = e.EmployeeId 
join Invoice i on i.CustomerId = c.CustomerId
group by c.Country`
const q13 = ``
const q14 = ``
const q15 = ``
const q16 = `SELECT TOP 1 Employee.LastName, Employee.FirstName
FROM Employee
INNER JOIN Customer ON Employee.EmployeeId = Customer.SupportRepId
INNER JOIN Invoice ON Customer.CustomerId = Invoice.CustomerId
GROUP BY Employee.FirstName, Employee.LastName
ORDER BY SUM(Invoice.Total)`
const q17 = ``
const q18 = `CREATE TABLE User
(
    id INT PRIMARY KEY NOT NULL,
    username VARCHAR(255),
	email VARCHAR(255),
	superuser BIT NOT NULL
);

CREATE TABLE Group
(
	id INT PRIMARY KEY NOT NULL,
	name VARCHAR(255),
	display_name VARCHAR(255),
	description VARCHAR(255)
);

CREATE TABLE User_Group
(
    user_id INT FOREIGN KEY REFERENCES User (id) NOT NULL,
    group_id INT FOREIGN KEY REFERENCES Group (id) NOT NULL,
);

CREATE TABLE Role
(
	id INT PRIMARY KEY NOT NULL,
	name VARCHAR(255),
	display_name VARCHAR(255),
	description VARCHAR(255)
);


CREATE TABLE User_Role
(
	user_id INT FOREIGN KEY REFERENCES User (id) NOT NULL,
	role_id INT FOREIGN KEY REFERENCES Role (id) NOT NULL
);

CREATE TABLE Group_Role
(
	group_id INT FOREIGN KEY REFERENCES Group (id) NOT NULL,
	role_id INT FOREIGN KEY REFERENCES Role (id) NOT NULL
);

CREATE TABLE Permission
(
	id INT PRIMARY KEY NOT NULL,
	name VARCHAR(255),
	display_name VARCHAR(255),
	description VARCHAR(255)
);

CREATE TABLE Role_Permission
(
	role_id INT FOREIGN KEY REFERENCES Role (id) NOT NULL,
	permission_id INT FOREIGN KEY REFERENCES Permission (id) NOT NULL
);`
const q19 = `INSERT INTO Track 
  (Name, 
  AlbumId, 
  MediaTypeId, 
  GenreId, 
  Composer, 
  Milliseconds, 
  Bytes, 
  UnitPrice) 
VALUES 
  ('New1', 10, 1, 1, 'Test', 33800, 2048, 0.99), 
  ('New2', 10, 1, 1, 'Test', 33800, 2048, 0.99), 
  ('New3', 10, 1, 1, 'Test', 33800, 2048, 0.99);
`
const q20 = `INSERT INTO Employee 
  (LastName, 
  FirstName, 
  Title, 
  ReportsTo,
  BirthDate, 
  HireDate, 
  Address, 
  City, 
  State, 
  Country, 
  PostalCode, 
  Phone, 
  Fax, 
  Email) 
VALUES 
  ('Vendee', 'Jules', 'Boss', 1, 19999-05-04, 2018-09-08, '89 Quais des Chartrons', 'Bordeaux', 'FR', 'France', '33800', '0600000000', '0000000000', 'jules.vendee@ynov.com'), 
  ('Test', 'EncoreTest', 'Stagiaire', 1, 1420-12-30, 2018-09-08, '89 Quais des Chartrons', 'Bordeaux', 'FR', 'France', '33800', '0600000001', '0000000001', 'test@ynov.com')`
const q21 = `DELETE FROM InvoiceLine
WHERE InvoiceId IN (
  SELECT InvoiceId 
  FROM Invoice 
  WHERE YEAR(InvoiceDate) = 2010
)
DELETE FROM Invoice
WHERE YEAR(InvoiceDate) = 2010`
const q22 = `UPDATE Invoice
SET CustomerId = (
  SELECT TOP 1 Customer.CustomerId
  FROM Customer
  JOIN Invoice ON Customer.CustomerId = Invoice.CustomerId
  WHERE Customer.Country = 'France'
  GROUP BY Customer.CustomerId
  ORDER BY COUNT(Invoice.InvoiceId) DESC
)
WHERE YEAR(InvoiceDate) >= 2011 
  AND YEAR(InvoiceDate) <= 2014
  AND BillingCountry = 'Germany'`
const q23 = `UPDATE Invoice
SET Invoice.BillingCountry = Customer.Country
FROM Invoice
JOIN Customer ON Invoice.CustomerId = Customer.CustomerId
WHERE Invoice.BillingCountry <> Customer.Country`
const q24 = `ALTER TABLE Employee 
ADD Salary int`
const q25 = `UPDATE Employee 
SET Salary = ROUND(RAND(CHECKSUM(NEWID()))*(100000-30000), 0) + 30000`
const q26 = `ALTER TABLE Invoice
DROP COLUMN BillingPostalCode
`











































// NE PAS TOUCHER CETTE SECTION
const tp = {name: name, promo: promo, queries: [q1, q2, q3, q4, q5, q6, q7, q8, q9, q10, q11, q12, q13, q14, q15, q16, q17, q18, q19, q20, q21, q22, q23, q24, q25, q26]}
module.exports = tp
